import { makeObservable, observable, action } from "mobx";

class Store {
  item_id: string = "item_1";
  constructor(){
    makeObservable(this, {
      item_id: observable,
      set_project_id: action
    })
  }
  setProjectId(item_id: string){
    this.item_id = item_id;
  }
  set_project_id = async (item_id: string) => {
    this.item_id=item_id
  }
}

const store = new Store();
export default store;