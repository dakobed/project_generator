from flask import Blueprint

blueprint = Blueprint('base', __name__)

@blueprint.route('/')
def index():
    return "index"