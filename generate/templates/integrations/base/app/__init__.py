from flask import Flask
from flask_cors import CORS
def create_app():
    app = Flask(__name__)
    CORS(
        app,
        origins=["http://localhost:8000"]
    )
    from app.views import mount_blueprints
    mount_blueprints(app)
    return app