from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
import os
import json

TEMPLATE_FOLDER = '/app/application/templates'
STATIC_FOLDER = '/app/application/static'

db = SQLAlchemy()

def create_app():
    app = Flask(__name__,
                template_folder=TEMPLATE_FOLDER,
                static_folder=STATIC_FOLDER
                )
    from app.views import mount_blueprints
    mount_blueprints(app)
    migrate = Migrate(app, db)
    # database_uri = ""
    #
    postgres_password = os.getenv('POSTGRES_PASSWORD')
    postgres_user = os.getenv('POSTGRES_USER')
    postgres_db = os.getenv('POSTGRES_DB')
    postgres_host = os.getenv('POSTGRES_HOST')
    postgres_port = os.getenv('POSTGRES_PORT')
    app.config.update(
        {
            "SQLALCHEMY_DATABASE_URI": f"postgresql://{postgres_user}:{postgres_password}@{postgres_host}:{postgres_port}/{postgres_db}"
        }
    )
    db.init_app(app)
    return app