from app.views.routes import blueprint

def mount_blueprints(app):
    app.register_blueprint(blueprint)