
from flask import Flask

def create_app():
    app = Flask(__name__)
    from app.views import mount_blueprints
    mount_blueprints(app)
    return app