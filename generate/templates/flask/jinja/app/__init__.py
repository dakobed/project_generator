
from flask import Flask

STATIC_FOLDER = '/app/app/static'
TEMPLATE_FOLDER = '/app/app/templates/'

def create_app():
    app = Flask(__name__,
                template_folder=TEMPLATE_FOLDER,
                static_folder=STATIC_FOLDER
                )
    from app.views import mount_blueprints
    mount_blueprints(app)
    return app