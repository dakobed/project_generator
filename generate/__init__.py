import click
import os
import shutil

CURRENT_DIRECTORY = os.getcwd()

@click.group()
@click.pass_context
def cli(ctx):
    """Example script."""
    pass

templates_path = os.path.join(os.path.dirname(__file__), 'templates')
print(templates_path)
template_names = os.listdir(templates_path)

project_names = [
    'react',
    'flask'
]
templates = {
    'react': {
        'nginx'
    },
    'flask': {
        'base',
        'postgres'
    }
}


@cli.command()
def list():
    click.echo("generate is a generator for initializing project templates")



@cli.command()
@click.option('--project', prompt=f"Which project would you like to create",type=click.Choice(project_names, case_sensitive=False))
def init(project, output=None):
    template = click.prompt(f"Which project {project} template would you like to use {templates[project]}",show_choices=templates[project])
    if not output:
        output = click.prompt("What directory would you like to save the project in")
    print(f"Selected {project}/{template} will be saved at {output}")
    selected_template_path = f"{templates_path}/{project}/{template}"
    create_directory(selected_template_path, output)

#
# @cli.command()
# @click.option('--template',
#               type=click.Choice(template_names, case_sensitive=False))
# @click.option('--project_name',
#               prompt='Project Name'
#               )
# def init(template, project_name):
#     print(template_names)
#     print(f"Selected {template}")
#     selected_template_path = templates_path+'/'+template
#     create_directory(selected_template_path, project_name)
#
def create_directory(template_path, newProjectPath):
    """
    Recursive copy of template files to new project directory
    :param template_path:
    :param newProjectPath:
    :return:
    """
    files_top_create = os.listdir(template_path)

    dirs = [file for file in files_top_create if os.path.isdir(f'{template_path}/{file}')]
    files = [file for file in files_top_create if os.path.isfile(f'{template_path}/{file}')]
    for dir in dirs:
        if f'{template_path}/{dir}'.endswith('__pycache__'):
            continue
        new_directory = f'{CURRENT_DIRECTORY}/{newProjectPath}/{dir}'
        os.makedirs(f'{CURRENT_DIRECTORY}/{newProjectPath}/{dir}', exist_ok=True)
        create_directory(f'{template_path}/{dir}', f"{newProjectPath}/{dir}")
    for file in files:
        source_file_path = f"{template_path}/{file}"
        output_file_path = f"{newProjectPath}/{file}"
        shutil.copy(source_file_path, output_file_path)
